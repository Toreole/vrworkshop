﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Toilette
{
    public class Dispenser : MonoBehaviour
    {
        [SerializeField]
        private GameObject rightToiletRoll;

        [SerializeField]
        private GameObject leftToiletRoll;

        [SerializeField]
        private string rightToiletRollTag;

        [SerializeField]
        private GameObject rightSpawn;

        [SerializeField]
        private GameObject leftSpawn;

        [SerializeField]
        private string leftToiletRollTag;

        private int rightcount;

        private int leftcount;

        [SerializeField]
        private int ToiletRollNumberRight;

        [SerializeField]
        private int ToiletRollNumberLeft;


        void Start()
        {
            Instantiate(rightToiletRoll, rightSpawn.transform.position, Quaternion.identity);
            Instantiate(leftToiletRoll, leftSpawn.transform.position, Quaternion.identity);
        }

        void Update()
        {
            rightcount = GameObject.FindGameObjectsWithTag(rightToiletRollTag).Length;

            leftcount = GameObject.FindGameObjectsWithTag(leftToiletRollTag).Length;
            
            if (rightcount < ToiletRollNumberRight)
            {
                Instantiate(rightToiletRoll, rightSpawn.transform.position, Quaternion.identity);
            }

            if (leftcount < ToiletRollNumberLeft)
            {
                Instantiate(leftToiletRoll, leftSpawn.transform.position, Quaternion.identity);
            }

        }
    }
}