﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Toilette
{
    public class ToiletPaperDespawn : MonoBehaviour
    {
        [SerializeField]
        private float secondsTillDespawn;

        [SerializeField]
        private string despawnWall;

        private bool goAhead;

        public void OnTriggerExit(Collider other)
        {
            if (other.CompareTag(despawnWall))
            {
                EnableDespawnTimer();
            }


        }

        void Start()
        {
            goAhead = false;
        }

        void EnableDespawnTimer()
        {
            goAhead = true;
        }

        void DespawnTimer()
        {
            secondsTillDespawn -= Time.deltaTime;
        }

        void Update()
        {
            if (goAhead == true)
            {
                DespawnTimer();
            }
            if (secondsTillDespawn <= 0)
            {
                Destroy(gameObject);
            }
            //Debug.Log(secondsTillDespawn);
        }
    }
}