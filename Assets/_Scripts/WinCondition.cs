﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
namespace Toilette
{
    public class WinCondition : MonoBehaviour
    {
        // timer of the game
        private float timer = 180;

        // Win picture if the player survived the 5 minutes 
        [SerializeField]
        private GameObject WinPicture;

        // Lose Picture if the player didn't survived the 5 minutes and got shot from the paparazzi
        [SerializeField]
        private GameObject LosePicture;

        // the text for the time
        [SerializeField]
        private TextMeshProUGUI timeText;

       /* [SerializeField]
        private GameObject mainMenu;
        public GameObject MainMenu
        {
            get
            {
                return mainMenu;
            }
            set
            {
                mainMenu = value;
            }

        }
        */


        private Enemy enemy;

        void Start()
        {
            WinPicture.SetActive(false);
            LosePicture.SetActive(false);
           // mainMenu.SetActive(false);
        }

        void Update()
        {
            timer -= Time.deltaTime;
            timeText.text = string.Format("Time left: {0:0}", timer);
            GameCondition();
        }

        void GameCondition()
        {
            // if the player survived 5 minutes the Win picture will appear
            if (timer == 0)
            {
                WinPicture.SetActive(true);
               // MainMenu.SetActive(true);
            }

            // if the player got shot from the paparazzi the Lose picture will appear
            if (timer > 0 && Enemy.HasWon)
            {
                LosePicture.SetActive(true);
                //MainMenu.SetActive(true);
            }
        }
    }
}