﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Toilette
{
    public class Enemy : MonoBehaviour
    {
        [SerializeField]
        private string TriggerTag;

        [SerializeField]
        private Transform moveDownDestination;

        [SerializeField]
        private Transform moveTopDestination;

        [SerializeField]
        private float WaitSecondsToGoUpAgain;

        [SerializeField]
        private float movementSpeed;

        private bool enemyMovesUp = false;
        bool shotPhoto = false;
        private Flash flash;

        static int playerLives = 3;
        public static bool HasWon { get; private set; }

        private bool enemyHasWon;
        public bool EnemyHasWon
        {
            get
            {
                return enemyHasWon;
            }
            set
            {
                enemyHasWon = value;
            }
        }
        // Start is called before the first frame update
        IEnumerator Start()
        {
            flash = FindObjectOfType<Flash>();
            yield return new WaitForSeconds(5);
            enemyMovesUp = true;
        }

        // Update is called once per frame
        void Update()
        {
            if (enemyHasWon)
                return;
            EnemyAppear();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Left") || other.CompareTag("Right") || other.CompareTag("Toiletpaper"))
            {
                enemyMovesUp = false;
                transform.position = moveDownDestination.position;
                StartCoroutine(EnemyMovement());
            }

        }
        private void EnemyAppear()
        {
            if (enemyMovesUp)
            {
                Vector3 TopPosition = Vector3.MoveTowards(transform.position, moveTopDestination.position, movementSpeed * Time.deltaTime);
                transform.position = TopPosition;
                if (shotPhoto)
                {
                    enemyMovesUp = false;
                    transform.position = moveDownDestination.position;
                    StartCoroutine(EnemyMovement());
                    return;
                }
                if (Vector3.Distance(transform.position, moveTopDestination.position) < 0.1f)
                {
                    shotPhoto = true;
                    playerLives--;
                    enemyHasWon = playerLives <= 0;
                    flash.doCameraFlash = true;
                    StartCoroutine(FlashLight());
                    if (enemyHasWon)
                    {
                        HasWon = true;
                    }
                }
            }
        }
        IEnumerator EnemyMovement()
        {
            yield return new WaitForSeconds(WaitSecondsToGoUpAgain);
            enemyMovesUp = true;
        }
        IEnumerator FlashLight()
        {
            yield return new WaitForSeconds(1f);
            flash.doCameraFlash = false;
            //StartCoroutine(FlashLight());       pls dont     
        }
    }
}