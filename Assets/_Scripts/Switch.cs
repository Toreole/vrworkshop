﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour
{
    float previous = 0;

    HingeJoint joint;

    public float lowGravity;
    public float normalGravity;

    private void Start()
    {
        joint = GetComponent<HingeJoint>();
    }

    private void FixedUpdate()
    {
        //print(joint.angle);
        if(!Mathf.Approximately(previous, joint.angle))
        {
            if(joint.angle >= joint.limits.max)
            {
                Physics.gravity = new Vector3(0, normalGravity, 0);
            }
            else if(joint.angle <= joint.limits.min)
            {
                Physics.gravity = new Vector3(0, lowGravity, 0);
            }
        }

        previous = joint.angle;
    }
}
