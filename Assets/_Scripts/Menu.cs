﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Toilette
{
    public class Menu : MonoBehaviour
    {
        [SerializeField]
        private string SceneName;

        private WinCondition winCondition;

        private void Start()
        {
            winCondition = FindObjectOfType<WinCondition>();
        }

        void RestartButton()
        {
            SceneManager.LoadScene(SceneName);
            //winCondition.MainMenu.SetActive(false);
        }
    }
}